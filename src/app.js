'use strict';

// requirements
const express = require('express');
const { User } = require('./user');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

app.get('/', (req, res) => {
    res.send('Post user preferences: name and email');
});

app.post('/', (req, res) => {
    Object.freeze(req.body);
    const whiteListKeys = ['email', 'name'];

    for(let key in req.body) {
        if(!whiteListKeys.includes(key)){
            res.status(400).send('Wrong request');
            return;
        }
    }
    if (req.body.email === undefined || req.body.name === undefined) {
        res.status(400).send('Please provide both email and name');
        return;
    }

    // we should check the length of the email and it's a string
    // we should validate if the email is correct eg. regex
    // we should check if name is A-Z with let say 30 characters limit

    var user = Object.assign(User, {email: req.body.email, name: req.body.name});
    console.log(user);
    res.send('Your preferences have been successfully saved');
});

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = app;
